USE [master]
GO
/****** Object:  Database [HardwareDB]    Script Date: 2021-03-26 10:54:02 ******/
CREATE DATABASE HardwareDB
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HardwareDB', FILENAME = N'你选择的文件存放路径\HardwareDB.mdf' , SIZE = 8960KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'HardwareDB_log', FILENAME = N'你选择的文件存放路径\HardwareDB_log.ldf' , SIZE = 10240KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [HardwareDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HardwareDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HardwareDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HardwareDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HardwareDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HardwareDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HardwareDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [HardwareDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HardwareDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [HardwareDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HardwareDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HardwareDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HardwareDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HardwareDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HardwareDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HardwareDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HardwareDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HardwareDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HardwareDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HardwareDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HardwareDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HardwareDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HardwareDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HardwareDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HardwareDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HardwareDB] SET RECOVERY FULL 
GO
ALTER DATABASE [HardwareDB] SET  MULTI_USER 
GO
ALTER DATABASE [HardwareDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HardwareDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HardwareDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HardwareDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [HardwareDB]
GO
/****** Object:  Table [dbo].[cloud_Devices]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cloud_Devices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceCloudId] [int] NULL,
	[DeviceName] [varchar](50) NULL,
	[Code] [varchar](50) NULL,
	[CategoryId] [int] NULL,
	[UseType] [varchar](50) NULL,
	[DeviceKey] [varchar](50) NULL,
	[Position] [varchar](50) NULL,
	[InOrOut] [int] NULL,
	[DeviceIp] [varchar](20) NULL,
	[DeviceType] [varchar](50) NULL,
	[RoomName] [varchar](50) NULL,
	[RoomId] [int] NULL,
	[State] [int] NULL,
	[VenueName] [varchar](50) NULL,
	[CompanyName] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](100) NULL,
 CONSTRAINT [PK_cloud_Devices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cloud_DevicesCategory]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cloud_DevicesCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[CategoryName] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_cloud_DevicesCatagory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cloud_member_log]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cloud_member_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MemberId] [varchar](50) NULL,
	[CardId] [varchar](50) NULL,
	[InTime] [varchar](50) NULL,
	[SN] [varchar](50) NULL,
	[BarcodIp] [varchar](50) NULL,
	[ReadNo] [varchar](50) NULL,
	[ComputerIp] [varchar](50) NULL,
	[ClassRoom] [varchar](50) NULL,
	[InOrOut] [varchar](50) NULL,
	[Flag] [varchar](50) NULL,
	[OnTime] [varchar](50) NULL,
 CONSTRAINT [PK_cloud_member_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_class_end]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_class_end](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[classid] [varchar](50) NULL,
	[coachname] [varchar](50) NULL,
	[coachid] [varchar](50) NULL,
	[coachok] [varchar](50) NULL,
	[classstatus] [varchar](50) NULL,
	[membername] [varchar](50) NULL,
	[membermobile] [varchar](50) NULL,
	[memberid] [varchar](50) NULL,
	[Nowtime] [varchar](50) NULL,
 CONSTRAINT [PK_Face_class_end] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_createAndDelete_msg]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_createAndDelete_msg](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[member] [varchar](50) NULL,
	[memberName] [varchar](50) NULL,
	[pic] [varchar](max) NULL,
	[admins] [varchar](50) NULL,
	[times] [datetime2](7) NULL,
	[types_] [varchar](50) NULL,
 CONSTRAINT [PK_Face_createAndDelete_msg] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_Details]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_Details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mobile] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[pic] [varchar](max) NULL,
	[UserTime] [datetime2](7) NULL,
	[flog] [varchar](50) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_Face_Details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_EquipmentIp]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_EquipmentIp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ip] [varchar](50) NULL,
	[EquipmentNo] [varchar](50) NULL,
	[state] [int] NULL,
	[remark] [varchar](50) NULL,
	[updates] [date] NULL,
	[create_time] [date] NULL,
	[inorout] [int] NULL,
	[name] [varchar](50) NULL,
	[category] [int] NULL,
	[classroom_id] [int] NULL,
	[face_type] [varchar](50) NULL,
 CONSTRAINT [PK_Face_Equipmentlp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_Histrory_Member]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_Histrory_Member](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[member] [varchar](50) NULL,
	[memberType] [varchar](50) NULL,
	[invalid_time] [varchar](50) NULL,
	[Date_Time] [varchar](50) NULL,
 CONSTRAINT [PK_Face_Histrory_Member] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_Histrory_Member_Count]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Face_Histrory_Member_Count](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[memberLeave] [int] NULL,
	[memberTerminate] [int] NULL,
	[memberRenew] [int] NULL,
	[dateTime] [datetime] NULL,
 CONSTRAINT [PK_Face_Histrory_Member_Count] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Face_LoginOn]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_LoginOn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[UserPassWord] [varchar](50) NULL,
	[AP] [int] NULL,
 CONSTRAINT [PK_Face_LoginOn_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_SendTime]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_SendTime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NodeType] [int] NULL,
	[SendUpTime] [date] NULL,
	[IntReamrk] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_Face_SendTime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Face_Temp_member_detail]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Face_Temp_member_detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mobile] [varchar](50) NULL,
	[name] [varchar](50) NOT NULL,
	[pic] [varchar](max) NULL,
	[UserTime] [datetime2](7) NULL,
	[ip] [varchar](50) NULL,
	[create_at] [datetime2](7) NULL,
	[error_num] [int] NULL,
 CONSTRAINT [PK_Face_Temp_member_detail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hd_category]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hd_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[c_id] [int] NOT NULL,
	[c_name] [varchar](50) NOT NULL,
	[c_info] [varchar](50) NOT NULL,
	[c_addtime] [datetime] NOT NULL,
 CONSTRAINT [PK__hd_categ__3213E83F375DC43C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hd_equipment]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hd_equipment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[e_id] [int] NOT NULL,
	[f_id] [int] NOT NULL,
	[c_id] [int] NOT NULL,
	[s_id] [int] NOT NULL,
	[e_name] [varchar](255) NOT NULL,
	[e_code] [varchar](50) NULL,
	[e_other_name] [varchar](255) NOT NULL,
	[e_ip] [varchar](255) NULL,
	[e_port] [int] NOT NULL,
 CONSTRAINT [PK__hd_equip__3213E83F4CFC7573] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hd_equipment_history_status]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hd_equipment_history_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ehs_id] [int] NULL,
	[ehs_status] [int] NULL,
	[ehs_status_name] [varchar](50) NULL,
	[ehs_time] [datetime] NULL,
	[ehs_info] [varchar](255) NULL,
 CONSTRAINT [PK__hd_equip__3213E83F451D259E] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_ClassRoom]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_ClassRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassRoomId] [varchar](20) NULL,
	[RoomId] [varchar](20) NULL,
	[RoomName] [varchar](50) NULL,
	[DeviceIp] [varchar](20) NULL,
	[DeviceSN] [varchar](50) NULL,
	[VenueId] [varchar](10) NULL,
	[RoomDescribe] [varchar](200) NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_ClassRoom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_CourseDetail]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_CourseDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AboutClassId] [int] NULL,
	[ClassId] [int] NOT NULL,
	[ClassName] [varchar](100) NULL,
	[ClassType] [tinyint] NULL,
	[RoomId] [int] NULL,
	[MemberId] [varchar](50) NOT NULL,
	[MemberName] [varchar](50) NULL,
	[MemberPhone] [varchar](20) NULL,
	[CoachId] [int] NOT NULL,
	[CoachName] [varchar](50) NULL,
	[ClassDate] [date] NULL,
	[ClassState] [smallint] NULL,
	[IsUpload] [smallint] NULL,
	[ClassStartTime] [datetime] NULL,
	[ClassEndTime] [datetime] NULL,
	[ClassStartStamp] [bigint] NULL,
	[ClassEndStamp] [bigint] NULL,
	[ClassLength] [int] NULL,
	[QuittingAllowTime] [int] NULL,
	[ActualStartTime] [datetime] NULL,
	[AllowEndStamp] [bigint] NULL,
	[ActualEndTime] [datetime] NULL,
	[IsOffLine] [smallint] NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [varchar](50) NULL,
	[UpdateNum] [smallint] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](500) NULL,
 CONSTRAINT [PK_local_CourseDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_DeviceUseRecords]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_DeviceUseRecords](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceIp] [varchar](20) NULL,
	[DeviceKey] [varchar](50) NULL,
	[DeviceState] [tinyint] NULL,
	[UseType] [varchar](50) NULL,
	[UseUrl] [varchar](100) NULL,
	[BackInfo] [varchar](max) NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](200) NULL,
 CONSTRAINT [PK_local_DeviceUseRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_FaceDiscernRecord]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_FaceDiscernRecord](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceKey] [varchar](50) NULL,
	[PersonId] [varchar](50) NULL,
	[Time] [varchar](20) NULL,
	[DeviceIp] [varchar](20) NULL,
	[Type] [varchar](20) NULL,
	[Path] [varchar](500) NULL,
	[Base64] [varchar](max) NULL,
	[UseCategory] [int] NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](500) NULL,
 CONSTRAINT [PK_local_FaceDiscernRecord] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_FacePicture]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_FacePicture](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonId] [varchar](50) NULL,
	[FTPPicPath] [varchar](500) NULL,
	[PicBase] [varchar](max) NULL,
	[LocalPicPath] [varchar](500) NULL,
	[CloudPicPath] [varchar](500) NULL,
	[PicType] [tinyint] NULL,
	[IsEnable] [tinyint] NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_Picture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_FaceRegedit]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_FaceRegedit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[deviceKey] [varchar](50) NULL,
	[person_Id] [varchar](50) NULL,
	[time] [varchar](13) NULL,
	[newImgPath] [varchar](500) NULL,
	[imgPath] [varchar](500) NULL,
	[faceId] [varchar](50) NULL,
	[ip] [varchar](20) NULL,
	[featureKey] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](100) NULL,
 CONSTRAINT [PK_local_FaceRegedit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_GroupCourseList]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_GroupCourseList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AboutCourseId] [int] NULL,
	[StartTimeStamp] [varchar](20) NULL,
	[EndTimeStamp] [varchar](23) NULL,
	[CourseDate] [varchar](30) NULL,
	[CourseId] [int] NULL,
	[CourseName] [varchar](50) NULL,
	[CoachId] [varchar](50) NULL,
	[CoachName] [varchar](50) NULL,
	[RoomId] [varchar](10) NULL,
	[IsSignIn] [int] NULL,
	[IsSignInTime] [varchar](20) NULL,
	[IsOffLine] [int] NULL,
	[IsUpload] [int] NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_GroupCourseList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_MemberChange]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_MemberChange](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MemberId] [varchar](50) NOT NULL,
	[ChangeType] [varchar](20) NULL,
	[CardStatus] [varchar](5) NULL,
	[CardId] [varchar](50) NULL,
	[CardNumber] [varchar](50) NULL,
	[LeaveId] [varchar](50) NULL,
	[LeaveStatus] [varchar](5) NULL,
	[LeaveStartTime] [varchar](13) NULL,
	[LeaveEndTime] [varchar](13) NULL,
	[InvalidTime] [varchar](13) NULL,
	[MemberName] [varchar](50) NULL,
	[CreateTime] [datetime2](3) NULL,
	[UpdateTime] [varchar](50) NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_MemberChange] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_MemberChangeLog]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_MemberChangeLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LeaveNum] [nchar](10) NULL,
	[CardLockNum] [nchar](10) NULL,
	[CardUseNum] [nchar](10) NULL,
	[RenewNum] [nchar](10) NULL,
	[TerminateNum] [nchar](10) NULL,
	[Code] [varchar](50) NULL,
	[CreateTime] [datetime2](3) NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](200) NULL,
 CONSTRAINT [PK_local_MemberChangeLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_MemberDetails]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_MemberDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MemberId] [varchar](20) NULL,
	[MemberName] [varchar](50) NULL,
	[MemberPhone] [varchar](20) NULL,
	[MemberStatus] [int] NULL,
	[InvalidDate] [datetime] NULL,
	[CardId] [varchar](50) NULL,
	[CardType] [int] NULL,
	[FacePicUrl] [varchar](500) NULL,
	[HeadImgUrl] [varchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[PicId] [int] NULL,
	[IsEnable] [tinyint] NULL,
	[VenueId] [int] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_MemberDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_OpenDoorLog]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_OpenDoorLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MemberId] [varchar](50) NULL,
	[MemberName] [varchar](50) NULL,
	[MemberCardId] [varchar](30) NULL,
	[DeviceIp] [varchar](20) NULL,
	[DeviceSn] [varchar](30) NULL,
	[TimeStamps] [varchar](15) NULL,
	[IsOffLine] [tinyint] NULL,
	[Status] [int] NULL,
	[IsUpload] [tinyint] NULL,
	[CreateTime] [datetime] NULL,
	[OpenTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL,
 CONSTRAINT [PK_local_OpenDoorLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_RequestRecords]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_RequestRecords](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MemberId] [int] NULL,
	[MemberName] [varchar](50) NULL,
	[RequestUrl] [varchar](500) NULL,
	[RecordType] [varchar](20) NULL,
	[BackInfo] [varchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](200) NULL,
 CONSTRAINT [PK_local_RequestRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_TimeNode]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_TimeNode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NodeType] [int] NULL,
	[TimeNode] [varchar](50) NULL,
	[TimeDisplay] [datetime] NULL,
	[CreateTime] [datetime] NULL,
	[IntRemark] [int] NULL,
	[StrRemark] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[local_Towels]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[local_Towels](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[mobile] [varchar](50) NULL,
	[cardid] [varchar](50) NULL,
	[ic_member] [varchar](50) NULL,
	[towelStyle] [varchar](50) NULL,
	[towelGetTime] [time](7) NULL,
	[towelSetTime] [time](7) NULL,
	[Date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[V_CardUse_Member]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CardUse_Member]
AS
SELECT  Id, MemberId, MemberName, MemberPhone, MemberStatus, InvalidDate, FacePicUrl, HeadImgUrl, CreateTime, PicId, 
                   IsEnable, VenueId, UpdateTime
FROM      dbo.local_MemberDetails AS md
WHERE   (MemberId IN
                       (SELECT DISTINCT MemberId
                        FROM       dbo.local_MemberChange AS mc
                        WHERE    (ChangeType = 'memberCardUse')))

GO
/****** Object:  View [dbo].[V_Course_RoomDevice]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Course_RoomDevice]
AS
SELECT  dbo.local_ClassRoom.RoomName, dbo.local_ClassRoom.DeviceSN, dbo.cloud_Devices.DeviceKey, 
                   dbo.local_ClassRoom.VenueId, dbo.cloud_Devices.VenueName, dbo.cloud_Devices.Code, dbo.cloud_Devices.DeviceIp, 
                   dbo.local_ClassRoom.RoomId, dbo.local_ClassRoom.ClassRoomId
FROM      dbo.local_ClassRoom INNER JOIN
                   dbo.cloud_Devices ON dbo.local_ClassRoom.RoomId = dbo.cloud_Devices.RoomId

GO
/****** Object:  View [dbo].[View_Face_CheckIn]    Script Date: 2021-03-26 10:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Face_CheckIn] AS SELECT
	dbo.Face_Histrory_Member.member,
	dbo.Face_Details.name,
	dbo.Face_Histrory_Member.memberType,
	dbo.Face_Histrory_Member.invalid_time,
	dbo.Face_Histrory_Member.Date_Time 
FROM
	dbo.Face_Details
	INNER JOIN dbo.Face_Histrory_Member ON dbo.Face_Details.mobile = dbo.Face_Histrory_Member.member
GO
ALTER TABLE [dbo].[cloud_Devices] ADD  CONSTRAINT [DF_cloud_Devices_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[cloud_DevicesCategory] ADD  CONSTRAINT [DF_cloud_DevicesCategory_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[Face_Details] ADD  CONSTRAINT [DF_Face_Details_create_time]  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[Face_EquipmentIp] ADD  CONSTRAINT [DF_Face_EquipmentIp_create_time]  DEFAULT (getdate()) FOR [create_time]
GO
ALTER TABLE [dbo].[Face_Histrory_Member_Count] ADD  CONSTRAINT [DF_Face_Histrory_Member_Count_dateTime]  DEFAULT (getdate()) FOR [dateTime]
GO
ALTER TABLE [dbo].[Face_Temp_member_detail] ADD  CONSTRAINT [DF_Face_Temp_member_detail_create_at]  DEFAULT (getdate()) FOR [create_at]
GO
ALTER TABLE [dbo].[hd_category] ADD  CONSTRAINT [DF_hd_category_c_addtime]  DEFAULT (getdate()) FOR [c_addtime]
GO
ALTER TABLE [dbo].[hd_equipment_history_status] ADD  CONSTRAINT [DF_hd_equipment_history_status_ehs_status]  DEFAULT ((0)) FOR [ehs_status]
GO
ALTER TABLE [dbo].[local_ClassRoom] ADD  CONSTRAINT [DF_local_ClassRoom_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_CourseDetail] ADD  CONSTRAINT [DF_local_CourseDetail_IsUpload]  DEFAULT ((0)) FOR [IsUpload]
GO
ALTER TABLE [dbo].[local_CourseDetail] ADD  CONSTRAINT [DF_local_CourseDetail_IsOffLine]  DEFAULT ((0)) FOR [IsOffLine]
GO
ALTER TABLE [dbo].[local_CourseDetail] ADD  CONSTRAINT [DF_CourseDetail_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_DeviceUseRecords] ADD  CONSTRAINT [DF_local_DeviceUseRecords_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_FaceDiscernRecord] ADD  CONSTRAINT [DF_local_FaceDiscernRecord_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_FacePicture] ADD  CONSTRAINT [DF_local_FacePicture_IsEnable]  DEFAULT ((1)) FOR [IsEnable]
GO
ALTER TABLE [dbo].[local_FacePicture] ADD  CONSTRAINT [DF_local_FacePicture_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_FaceRegedit] ADD  CONSTRAINT [DF_FaceRegedit_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_GroupCourseList] ADD  CONSTRAINT [DF_local_GroupCourseList_IsSignIn]  DEFAULT ((0)) FOR [IsSignIn]
GO
ALTER TABLE [dbo].[local_GroupCourseList] ADD  CONSTRAINT [DF_local_GroupCourseList_IsOffLine]  DEFAULT ((0)) FOR [IsOffLine]
GO
ALTER TABLE [dbo].[local_GroupCourseList] ADD  CONSTRAINT [DF_local_GroupCourseList_IsUpload]  DEFAULT ((0)) FOR [IsUpload]
GO
ALTER TABLE [dbo].[local_GroupCourseList] ADD  CONSTRAINT [DF_local_GroupCourseList_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_MemberChange] ADD  CONSTRAINT [DF_local_MemberChange_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_MemberChangeLog] ADD  CONSTRAINT [DF_local_MemberChangeLog_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_MemberDetails] ADD  CONSTRAINT [DF_local_MemberDetails_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_MemberDetails] ADD  CONSTRAINT [DF_local_MemberDetails_IsEnable]  DEFAULT ((1)) FOR [IsEnable]
GO
ALTER TABLE [dbo].[local_OpenDoorLog] ADD  CONSTRAINT [DF_local_OpenDoorLog_IsOffLine]  DEFAULT ((0)) FOR [IsOffLine]
GO
ALTER TABLE [dbo].[local_OpenDoorLog] ADD  CONSTRAINT [DF_local_OpenDoorLog_IsUpload]  DEFAULT ((0)) FOR [IsUpload]
GO
ALTER TABLE [dbo].[local_OpenDoorLog] ADD  CONSTRAINT [DF_local_OpenDoorLog_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_RequestRecords] ADD  CONSTRAINT [DF_local_RequestRecords_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[local_TimeNode] ADD  CONSTRAINT [DF_local_TimeNode_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备编码，平台生成后添加' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用类型：现有：团课，私教，进出馆' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'UseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'硬件编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'DeviceKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'进门或者出门，0出门1进门' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'InOrOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备IP地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'DeviceIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备型号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'DeviceType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教室名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'RoomName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教室ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'RoomId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_Devices', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'团课进出记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扫码或手环时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'InTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'板子SN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'SN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'板子IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'BarcodIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'门号，前后门，1、2等' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'ReadNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'黑盒子IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'ComputerIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教室' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'ClassRoom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1进0出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'InOrOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开门时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cloud_member_log', @level2type=N'COLUMN',@level2name=N'OnTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上下课记录日志（成功失败都记录）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'coachname'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练是否验证' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'coachok'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'classstatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'membername'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_class_end', @level2type=N'COLUMN',@level2name=N'Nowtime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员人脸创建删除记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员编号，m开头会员，其他员工' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'member'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片base64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'pic'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作员账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'admins'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'times'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_createAndDelete_msg', @level2type=N'COLUMN',@level2name=N'types_'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片信息所有' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Details', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态，暂时不用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Details', @level2type=N'COLUMN',@level2name=N'flog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类别 1，闸机的人脸识别，2，私教上下课的人脸识别，3，团课的人脸识别，4，vip浴室的人脸识别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_EquipmentIp', @level2type=N'COLUMN',@level2name=N'category'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员更换照片记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Histrory_Member', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Histrory_Member', @level2type=N'COLUMN',@level2name=N'invalid_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Histrory_Member', @level2type=N'COLUMN',@level2name=N'Date_Time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'本地管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_LoginOn', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1管理员（增加员工，删除员工），其他是员工' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_LoginOn', @level2type=N'COLUMN',@level2name=N'AP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1一体机数据同步节点' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_SendTime', @level2type=N'COLUMN',@level2name=N'NodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'获取本场馆会员信息是时间，获取的时候带上，并更新' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_SendTime', @level2type=N'COLUMN',@level2name=N'SendUpTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'本地注册人脸，需同步到人脸识别仪的临时数据，同步完成删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Temp_member_detail', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'人脸识别仪的IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Face_Temp_member_detail', @level2type=N'COLUMN',@level2name=N'ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'硬件类型表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_category', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_category', @level2type=N'COLUMN',@level2name=N'c_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_category', @level2type=N'COLUMN',@level2name=N'c_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_category', @level2type=N'COLUMN',@level2name=N'c_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_category', @level2type=N'COLUMN',@level2name=N'c_addtime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hd_equipment', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教室编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_ClassRoom', @level2type=N'COLUMN',@level2name=N'RoomId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教室名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_ClassRoom', @level2type=N'COLUMN',@level2name=N'RoomName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_ClassRoom', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_ClassRoom', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程类型，1会员私教2会员团课3员工团课' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'MemberId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'MemberName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'CoachId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'CoachName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程状态，0:初始 1:上课 2:下课 3:取消 4:爽约 5:下课未打卡（团课状态：0:待上课 1:上课中 2:已完结 3:未上课4:已签到）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassState'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否上传，0未上传1已上传，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'IsUpload'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程开始时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassStartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程结束时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassEndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程开始时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassStartStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程结束时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassEndStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程时长(秒)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ClassLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许提前下课时间，秒' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'QuittingAllowTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际结束时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ActualStartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'允许下课时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'AllowEndStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实际开始时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'ActualEndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是离线数据，0否1是，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'IsOffLine'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'UpdateNum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列，使用需注明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列，使用需注明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_CourseDetail', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'DeviceIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'DeviceKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'DeviceState'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用类型，团课，私教等' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'UseType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'访问的URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'UseUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'BackInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间,默认当前时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_DeviceUseRecords', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用类型,1进出馆2私教3团课4VIP室' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FaceDiscernRecord', @level2type=N'COLUMN',@level2name=N'UseCategory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'人员编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FacePicture', @level2type=N'COLUMN',@level2name=N'PersonId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片base64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FacePicture', @level2type=N'COLUMN',@level2name=N'PicBase'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片路径，网络路径或本地路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FacePicture', @level2type=N'COLUMN',@level2name=N'CloudPicPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片类型，1人脸照片2临时人脸照片3记录性照片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FacePicture', @level2type=N'COLUMN',@level2name=N'PicType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否可用，1是0否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_FacePicture', @level2type=N'COLUMN',@level2name=N'IsEnable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'约课编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'AboutCourseId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'StartTimeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'EndTimeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CourseDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CourseId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'课程名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CourseName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CoachId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CoachName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'教练时候验证，0否1是，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'IsSignIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是离线数据，0否1是，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'IsOffLine'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否上传，0否1是，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'IsUpload'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_GroupCourseList', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'变动类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberChange', @level2type=N'COLUMN',@level2name=N'ChangeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡状态：2正常3锁卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberChange', @level2type=N'COLUMN',@level2name=N'CardStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberChange', @level2type=N'COLUMN',@level2name=N'CardId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请假状态：2同意' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberChange', @level2type=N'COLUMN',@level2name=N'LeaveStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'MemberId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'MemberName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'MemberPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'失效日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'InvalidDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'照片ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'PicId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用，0否1是，默认1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'IsEnable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_MemberDetails', @level2type=N'COLUMN',@level2name=N'StrRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'MemberId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'MemberName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'会员卡号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'MemberCardId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'DeviceIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设备码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'DeviceSn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间戳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'TimeStamps'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否离线，0否1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'IsOffLine'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态，是否上传，0否1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'离线数据是否上传，0否1是，默认0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_OpenDoorLog', @level2type=N'COLUMN',@level2name=N'IsUpload'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'人员编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'MemberId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'人员姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'MemberName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'RequestUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'RecordType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'BackInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留列，使用需注明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_RequestRecords', @level2type=N'COLUMN',@level2name=N'IntRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_TimeNode', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'节点类型，1离线课程提交2进馆记录3私教课程获取4团课课程获取5会员获取6员工获取7人员信息变动' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_TimeNode', @level2type=N'COLUMN',@level2name=N'NodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间节点' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_TimeNode', @level2type=N'COLUMN',@level2name=N'TimeNode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'毛巾领取记录' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手环号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'ic_member'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'毛巾类别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'towelStyle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'领取时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'towelGetTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'归还时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'towelSetTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'local_Towels', @level2type=N'COLUMN',@level2name=N'Date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[23] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "md"
            Begin Extent = 
               Top = 7
               Left = 304
               Bottom = 230
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1632
         Width = 1800
         Width = 1200
         Width = 1200
         Width = 1644
         Width = 1200
         Width = 948
         Width = 1116
         Width = 1596
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CardUse_Member'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CardUse_Member'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[19] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -20
      End
      Begin Tables = 
         Begin Table = "cloud_Devices"
            Begin Extent = 
               Top = 6
               Left = 947
               Bottom = 404
               Right = 1187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "local_ClassRoom"
            Begin Extent = 
               Top = 9
               Left = 655
               Bottom = 343
               Right = 858
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1320
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Course_RoomDevice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Course_RoomDevice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Face_Details"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Face_Histrory_Member"
            Begin Extent = 
               Top = 6
               Left = 220
               Bottom = 205
               Right = 386
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Face_CheckIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Face_CheckIn'
GO
USE [master]
GO
ALTER DATABASE [HardwareDB] SET  READ_WRITE 
GO
